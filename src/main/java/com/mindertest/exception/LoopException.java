package com.mindertest.exception;

public class LoopException extends Exception{

    private String message;

    public LoopException (String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
