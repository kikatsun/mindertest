package com.mindertest.test;

import com.mindertest.dataccess.PlantillasManager;
import com.mindertest.exception.LoopException;
import com.mindertest.model.Plantilla;

public class MinderTest {

    public static Plantilla updatePlantilla (Long id, String name, String font, String color, Integer fontSize, Plantilla base) throws Exception {

        PlantillasManager manager = PlantillasManager.getInstance();

        Plantilla plantilla = manager.find(id);
        if (plantilla != null) {
            Plantilla test = new Plantilla();
            test.setId(id);
            test.setName(name);
            test.setFont(font);
            test.setColor(color);
            test.setFontSize(fontSize);
            test.setBase(base);

            plantilla.setName((String) PlantillaController.evaluateAttribute(test, PlantillaController.Attribute.NAME));
            plantilla.setFont((String) PlantillaController.evaluateAttribute(test, PlantillaController.Attribute.FONT));
            plantilla.setColor((String) PlantillaController.evaluateAttribute(test, PlantillaController.Attribute.COLOR));
            plantilla.setFontSize((Integer) PlantillaController.evaluateAttribute(test, PlantillaController.Attribute.FONT_SIZE));
            manager.update(id, plantilla);
        } else {
            throw new Exception ("Could not find Plantilla by id: "+id);
        }
        return plantilla;
    }

}
