package com.mindertest.test;

import com.mindertest.exception.LoopException;
import com.mindertest.model.Plantilla;
import org.hcjf.utils.Introspection;

import java.util.ArrayList;
import java.util.List;

public class PlantillaController {

    /**
     * Evalúa el atributo de la plantilla, en caso sea nulo lo busca en la plantilla base sin que se genere un ciclo
     * infinito, de ser así lanza una excepción.
     * @param plantilla
     * @param attribute
     * @return
     * @throws LoopException
     */
    public static Object evaluateAttribute(Plantilla plantilla, Attribute attribute) throws LoopException {
        Object value = Introspection.get(plantilla, attribute.attributeName);
        if (value == null) {
            List<Long> ids = new ArrayList<>();
            ids.add(Introspection.get(plantilla, Attribute.ID.attributeName));
            Plantilla base = plantilla.getBase();
            Long currentId = Introspection.get(base, Attribute.ID.attributeName);
            while (!ids.contains(currentId)) {
                ids.add(currentId);
                value = Introspection.get(base, attribute.attributeName);
                if (value == null) {
                    base = base.getBase();
                    currentId = Introspection.get(base, Attribute.ID.attributeName);
                } else {
                    break;
                }
            }
            if (value == null && ids.contains(currentId)) {
                throw new LoopException("There are cyclic attribute: " +attribute.attributeName);
            }
        }
        return value;
    }

    public enum Attribute {
        ID("id"),
        NAME("name"),
        FONT("font"),
        COLOR("color"),
        FONT_SIZE("fontSize");

        String attributeName;

        Attribute(String attributeName) {
            this.attributeName = attributeName;
        }
    }

}
