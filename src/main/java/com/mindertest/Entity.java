package com.mindertest;

import java.util.UUID;

public abstract class Entity {

    protected UUID _id;

    public UUID get_id() {
        return _id;
    }

    public void set_id(UUID _id) {
        this._id = _id;
    }
}
