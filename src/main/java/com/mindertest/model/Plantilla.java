package com.mindertest.model;

import com.mindertest.Entity;

public class Plantilla extends Entity {

    private Long id;
    private String name;
    private String font;
    private String color;
    private Integer fontSize;
    private Plantilla base;

    public Plantilla (Long id, String name, String font, String color, Integer fontSize) {
        this.id = id;
        this.name = name;
        this.font = font;
        this.color = color;
        this.fontSize = fontSize;
    }

    public Plantilla () {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFont() {
        return font;
    }

    public void setFont(String font) {
        this.font = font;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getFontSize() {
        return fontSize;
    }

    public void setFontSize(Integer fontSize) {
        this.fontSize = fontSize;
    }

    public Plantilla getBase() {
        return base;
    }

    public void setBase(Plantilla base) {
        this.base = base;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("{")
                .append("\"id\":")
                .append(id).append(",")
                .append("\"name\":")
                .append(name).append(",")
                .append("\"font\":")
                .append(font).append(",")
                .append("\"color\":")
                .append(color).append(",")
                .append("\"fontSize\":")
                .append(fontSize).append(",")
                .append("\"base\":");
                if (base != null) {
                result.append(base.getId());
                } else {
                    result.append("null");
                }
                result.append("}");
        return result.toString();
    }
}
