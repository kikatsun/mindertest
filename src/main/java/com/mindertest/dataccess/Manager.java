package com.mindertest.dataccess;

import com.mindertest.Entity;

import java.util.Collection;

public interface Manager<E extends Entity, K> {

    /**
     * Find Entity by some attribute/s.
     * @param key
     * @return
     */
    public E find (K key);

    /**
     * Find all entities.
     * @return
     */
    public Collection<E> findAll ();

    /**
     * Insert new Entity.
     * @param key
     * @param entity
     * @return
     */
    public E insert (K key, E entity);

    /**
     * Update Entity.
     * @param key
     * @param entity
     * @return
     */
    public E update (K key, E entity);

    /**
     * Delete Entity.
     * @param key
     * @return
     */
    public E delete (K key);

}
