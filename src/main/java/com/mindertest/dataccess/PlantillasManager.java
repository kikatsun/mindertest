package com.mindertest.dataccess;

import com.mindertest.model.Plantilla;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class PlantillasManager implements Manager<Plantilla, Long> {

    public static PlantillasManager instance;

    private static Map<Long, Plantilla> memory;

    private PlantillasManager() {
        memory =  new HashMap();
    }

    public static PlantillasManager getInstance() {
        if (instance == null) {
            instance = new PlantillasManager();
        }
        return instance;
    }

    public Plantilla find(Long key) {
        return memory.get(key);
    }

    public Collection<Plantilla> findAll() {
        return memory.values();
    }

    public Plantilla insert(Long key, Plantilla entity) {
        return memory.put(key, entity);
    }

    public Plantilla update(Long key, Plantilla entity) {
        return memory.put(key, entity);
    }

    public Plantilla delete(Long key) {
        return memory.remove(key);
    }

    public void clearMemory () {
        memory = new HashMap<>();
    }

}
