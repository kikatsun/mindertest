import com.mindertest.dataccess.PlantillasManager;
import com.mindertest.exception.LoopException;
import com.mindertest.model.Plantilla;
import org.junit.Assert;
import org.junit.Test;

public class MinderTest {

    Plantilla plantilla1 = new Plantilla(25l, "Plantilla1", "Comic Sans", "9521C4", 14);
    Plantilla plantilla2 = new Plantilla(26l, "Plantilla2", "Verdana", "3F56DE", null);
    Plantilla plantilla3 = new Plantilla(27l, "Plantilla3", "Arial", null, null);
    Plantilla plantilla4 = new Plantilla(55l, null, null, null, null);
    Plantilla plantilla5 = new Plantilla(56l, null, null, null, null);
    Plantilla plantilla6 = new Plantilla(57l, null, null, null, null);


    public void init () {
        PlantillasManager.getInstance().clearMemory();
        plantilla1.setBase(null);
        PlantillasManager.getInstance().insert(plantilla1.getId(), plantilla1);
        plantilla2.setBase(plantilla3);
        PlantillasManager.getInstance().insert(plantilla2.getId(), plantilla2);
        plantilla3.setBase(plantilla2);
        PlantillasManager.getInstance().insert(plantilla3.getId(), plantilla3);
        PlantillasManager.getInstance().insert(plantilla4.getId(), plantilla4);
        plantilla5.setBase(plantilla4);
        PlantillasManager.getInstance().insert(plantilla5.getId(), plantilla5);
        plantilla6.setBase(plantilla5);
        PlantillasManager.getInstance().insert(plantilla6.getId(), plantilla6);

    }

    /**
     * Test básico de un update de una atributo que no genera una excepción cíclica.
     */
    @Test
    public void updatePlantillaTest ()  throws Exception{
        init();
        Plantilla spected = plantilla1;
        spected.setName("plantilla 55");
        spected.setBase(plantilla3);

        Plantilla updated = null;
        LoopException loopException = null;
        try {
            updated = com.mindertest.test.MinderTest.updatePlantilla(spected.getId(), spected.getName(),
                    spected.getFont(), spected.getColor(), spected.getFontSize(), plantilla3);
        } catch (LoopException lex) {
            loopException = lex;
        }
        Assert.assertNotNull(updated);
        Assert.assertEquals(spected.toString(), updated.toString());
        Assert.assertNull(loopException);

    }

    /**
     * Test del update de la plantilla que no genera una excepción cíclica.
     */
    @Test
    public void updatePlantillaTest2 () throws Exception {
        init();
        Plantilla spected = plantilla1;
        spected.setBase(plantilla1);

        Plantilla updated = null;
        LoopException loopException = null;
        try {
            updated = com.mindertest.test.MinderTest.updatePlantilla(spected.getId(), spected.getName(),
                spected.getFont(), spected.getColor(), spected.getFontSize(), plantilla1);
        } catch (LoopException lex) {
            loopException = lex;
        }
        Assert.assertNotNull(updated);
        Assert.assertEquals(spected.toString(), updated.toString());
        Assert.assertNull(loopException);

    }

    /**
     * Test del update de un atributo y la plantilla que genera una excepción cíclica.
     */
    @Test
    public void updatePlantillaTest3 () throws Exception {
        init();
        Plantilla spected = plantilla1;
        spected.setName(null);
        spected.setBase(plantilla1);

        Plantilla updated = null;
        LoopException loopException = null;
        try {
            updated  = com.mindertest.test.MinderTest.updatePlantilla(spected.getId(), spected.getName(),
            spected.getFont(), spected.getColor(), spected.getFontSize(), plantilla1);
        } catch (LoopException lex) {
            loopException = lex;
        }
        Assert.assertNull(updated);
        Assert.assertNotNull("There are cyclic attribute", loopException);

    }

    /**
     * Test del update de la Plantilla base  que genera una excepción cíclica.
     */
    @Test
    public void updatePlantillaTest4 () throws Exception {
        init();
        Plantilla spected = plantilla4;
        spected.setBase(plantilla2);
        Plantilla updated = null;
        LoopException loopException = null;
        try {
            updated = com.mindertest.test.MinderTest.updatePlantilla(spected.getId(), spected.getName(),
                    spected.getFont(), spected.getColor(), spected.getFontSize(), plantilla2);
        } catch (LoopException lex) {
            loopException = lex;
        }
        Assert.assertNotNull("There are cyclic attribute", loopException);

    }

    /**
     * Test del update de la Plantilla base  que genera una excepción cíclica.
     */
    @Test
    public void updatePlantillaTest5 () throws Exception {
        init();
        Plantilla spected = plantilla4;
        spected.setId(456789l);
        spected.setBase(plantilla2);
        Plantilla updated = null;
        LoopException loopException = null;
        Exception exception = null;
        try {
            updated = com.mindertest.test.MinderTest.updatePlantilla(spected.getId(), spected.getName(),
                    spected.getFont(), spected.getColor(), spected.getFontSize(), plantilla2);
        } catch (LoopException lex) {
            loopException = lex;
        } catch (Exception ex) {
            exception = ex;
        }
        Assert.assertNull("There are not LoopException", loopException);
        Assert.assertNotNull("There are exception", exception);

    }

}
