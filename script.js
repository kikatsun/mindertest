var alumnos = [
    {
        legajo: 27,
        nombre: 'Cornelio Saavedra',
        notas: [5,2,9,10],
        promedio: null
    },
    {
        legajo: 54,
        nombre: 'Juan Manuel de Rosas',
        notas: [5,10,7,7],
        promedio: null
    },
    {
        legajo: 11,
        nombre: 'Bernardino Rivadavia',
        notas: [1,1,7,5],
        promedio: null
    },
    {
        legajo: 35,
        nombre: 'Bartolomé Mitre',
        notas: [7,7,5,9],
        promedio: null
    }

];


var minLegajo = 1, maxLegajo = 100;

for(var i=minLegajo; i <= maxLegajo; i++) {
    var alumno = buscarAlumnoPorLegajo(i);
    if( alumno != null ) {
        var notas = alumno.notas;
        alumno.promedio = calcularPromedio( notas, alumno.promedio );
        if(alumno.legajo == 11) {
            console.log("Nombre y Apellido: "+alumno.nombre);
            // Salida:
            console.log("Notas: "+alumno.notas);
            // Salida:
            console.log("Promedio: "+alumno.promedio);
            // Salida:
        }
    }
}

// Funciones auxiliares

function calcularPromedio (notas, promedio) {
    if( promedio == null) {
        var sumatoria = 0;
        for(var i=0; i< notas.length; i++) {
            sumatoria += notas[i];
            //notas[i] = -1;
        }
        return promedio = sumatoria / notas.length;
    }
}

function buscarAlumnoPorLegajo (legajo) {
    var alumno;
    for(var i=0; i<alumnos.length; i++) {
        if (alumnos[i].legajo == legajo) {
            alumno=alumnos[i];
            break;
        }
    }
    return alumno;
}